import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'UsersView',
    component: () => import('../views/UsersView.vue')
  },
  {
    path: '/teachers',
    name: 'TeachersView',
    component: () => import('../views/TeachersView.vue')
  },
  {
    path: '/students',
    name: 'StudentsView',
    component: () => import('../views/StudentsView.vue')
  },
  {
    path: '/:id',
    name: 'UserView',
    component: () => import('../views/UserView.vue')
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router

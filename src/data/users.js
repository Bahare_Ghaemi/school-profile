export default [
    {
        username: 'ali_gh63',
        name: 'Ali Ghasemi',
        email: 'ali.ghasemi92@gmail.com',
        role: 'teacher',
        image: require('../assets/avatar.png')
    },
    {
        username: 'ehsan_amiri_4145',
        name: 'Ehsan Amiri',
        email: 'ehsan.amiri__326@gmail.com',
        role: 'student',
        image: require('../assets/avatar.png')
    },
    {
        username: 'reza__62',
        name: 'Reza Moradi',
        email: 'reza.moradi@gmail.com',
        role: 'teacher',
        image: require('../assets/avatar.png')
    },
    {
        username: 'amir__mohamadi74',
        name: 'Amir Mohammadi',
        email: 'amir.mohamadi_586@gmail.com',
        role: 'student',
        image: require('../assets/avatar.png')
    },
    {
        username: 'zahra_rezaee_175',
        name: 'Zahra Rezaee',
        email: 'zahra.rezaee52@gmail.com',
        role: 'student',
        image: require('../assets/avatar-woman.png')
    },
    {
        username: 'mohamad_azz54',
        name: 'Mohammad Azimi',
        email: 'mohammad.azz1@gmail.com',
        role: 'teacher',
        image: require('../assets/avatar.png')
    },
    {
        username: 'sahar_amjd__581',
        name: 'Sahar Amjadi',
        email: 'sahar.amjadi412@gmail.com',
        role: 'student',
        image: require('../assets/avatar-woman.png')
    },
    {
        username: 'amin__jvd128',
        name: 'Amin Javadi',
        email: 'amin.javadi816@gmail.com',
        role: 'student',
        image: require('../assets/avatar.png')
    },
    {
        username: 'fatme__krmi55',
        name: 'Fatemeh Karimi',
        email: 'fa.karimi124@gmail.com',
        role: 'teacher',
        image: require('../assets/avatar-woman.png')
    },
    {
        username: 'sara_mhdp_853',
        name: 'Sara Mahdizadeh',
        email: 'sara.mahdizde4263@gmail.com',
        role: 'student',
        image: require('../assets/avatar-woman.png')
    },
    {
        username: 'sajad_mlk924',
        name: 'Sajad Maleki',
        email: 'sajad.maleki_145@gmail.com',
        role: 'student',
        image: require('../assets/avatar.png')
    },
]